#! /bin/bash
#
# Move files in/out of IPOPP folders
# Requires inotify-tools
#

mydir="$(readlink -f $(dirname $0))"
myname="$(basename $0)"
FILE="${FILE:-${mydir}/inotify.txt}"
LOG=/tmp/inotify.log

set -ux
kill -TERM $(pgrep -f inotify | grep -v ^$$\$)

inotifywait -m --fromfile $FILE | while read _d _e _f; do
  case $_d in
    {{ ftp_root }}/ipopp/modis*)
      if [ "$_e" = "MOVED_TO" ] || [ "$_e" = "CLOSE_WRITE" ] || [ "$_e" = "CLOSE_WRITE,CLOSE" ]; then
        echo /home/ipopp/scripts/move_to_ipopp.sh {{ ipopp_docker_dir }}/modis_raid/dsm/nisfes_data "${_d}${_f}" >>$LOG
        /home/ipopp/scripts/move_to_ipopp.sh {{ ipopp_docker_dir }}/modis_raid/dsm/nisfes_data "${_d}${_f}" >>$LOG 2>&1
      fi
      ;;
    {{ ipopp_docker_dir }}/modis_raid/pub/gsfcdata/*/modis/level2*)
      if [ "$_e" = "MOVED_TO" ] || [ "$_e" = "CLOSE_WRITE" ] || [ "$_e" = "CLOSE_WRITE,CLOSE" ]; then
        echo /home/ipopp/scripts/send_mod14.sh "${_d}${_f}" >>$LOG
        /home/ipopp/scripts/send_mod14.sh "${_d}${_f}" >>$LOG 2>&1
      fi
      ;;
    {{ ipopp_docker_dir }}/npp_raid/pub/gsfcdata/npp/viirs/level2*)
      if [ "$_e" = "MOVED_TO" ] || [ "$_e" = "CLOSE_WRITE" ] || [ "$_e" = "CLOSE_WRITE,CLOSE" ]; then
        echo /home/ipopp/scripts/send_viirs.sh "${_d}${_f}" >>$LOG
        /home/ipopp/scripts/send_viirs.sh "${_d}${_f}" >>$LOG 2>&1
      fi
      ;;
    *)
      echo "No handler defined for ${_d}${_f}" >&2
      echo "No handler defined for ${_d}${_f}" >>$LOG
      ;;
  esac
done
