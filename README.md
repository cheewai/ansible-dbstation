# ansible-dbstation

Ansible script to prepare a server as MODIS/NPP direct-broadcast receiving station

## Usage

* To access the IPOPP Java GUI, log in to the X windows as *ipopp* and run the following:

```
docker exec -it ipopp_npp su - ipopp /home/ipopp/drl/tools/dashboard.sh
```

## Caveat

* To run the IPOPP java GUI, the docker container has to run with *net: "host"* set in *docker-compose.yml* which causes the container to not have a separate networking stack. But it also means that only ONE instance of IPOPP may run. This may be useful if one needs the GUI to configure IPOPP. But not for normal operation.

